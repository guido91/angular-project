# FinalProject



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Dependency
* chat-gateway
* registy-service
* chat-user
* chat-contact
* auth-service

## Point includes into project
- Modularización (Routing) (15puntos)
- Lazy loading modules     (10 puntos)
- Guards                   (5 puntos)
- Servicios                (5 puntos)
- Interceptores            (5 puntos)
- LocalStorage             (5 puntos)
- Formularios reactivos    (10 puntos)
- Carga por demanda (paginación) (5 puntos)
- POST. Create Company (account) (5 puntos)
- POST. Authentication           (5 puntos)
- POST. Create contact           (10 puntos)
- POST. Search                   (10 puntos)

## Change to test
#### environment
this IP can be IP and PORT of chat-gateway

        export const environment = {
          production: false,
          api: {
            host: 'http://localhost:9090'
          }
        };
