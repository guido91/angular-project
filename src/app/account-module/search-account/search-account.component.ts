import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SearchAccountService} from '../services/search-account.service';

@Component({
    selector: 'app-search-account',
    templateUrl: './search-account.component.html',
    styleUrls: ['./search-account.component.scss']
})
export class SearchAccountComponent implements OnInit {

    public searchAccountFormGroup: FormGroup;
    public account: any;
    constructor(private formBuilder: FormBuilder,
                private searchAccountService: SearchAccountService) {
        this.searchAccountFormGroup = this.formBuilder.group({
            account: ['', [Validators.required]]
        });
    }

    ngOnInit() {
    }

    onSearchAccount() {
      console.log(this.searchAccountFormGroup.value);
      this.searchAccountService.doGet('ACTIVATED', this.searchAccountFormGroup.value.account).subscribe(
          (response) => {
              console.log(response);
              this.account = response;
          }
      );
    }

}
