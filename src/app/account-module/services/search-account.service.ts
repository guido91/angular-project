import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../../bootstrap/http.service';
import {Observable} from 'rxjs';

@Injectable()
export class SearchAccountService extends HttpService {

    constructor(private _injector: Injector) {
        super();
    }

    public path(): string {
        return '/api/users/secure/accounts';
    }

    public doGet(state: string, email: string): Observable<object> {
        console.log(email);
        return this.httpClient().get(`${this.getUrl()}?state=${state}&email=${email}`);
    }

    protected injector(): Injector {
        return this._injector;
    }
}
