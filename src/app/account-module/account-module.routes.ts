import {Routes} from '@angular/router';
import {SearchAccountComponent} from './search-account/search-account.component';
import {MainAccountComponent} from './main-account/main-account.component';
import {AuthGuard} from '../routing-module/guards/auth.guard';

export const ACCOUNT_ROUTES: Routes = [
    {
        path: '',
        component: MainAccountComponent,
        children: [
            {
                path: 'search',
                component: SearchAccountComponent,
                canLoad: [AuthGuard]
            },
            /*{
                path: 'sign-up',
                component: SecureLoginComponent
            },
            {
                path: '',
                redirectTo: '/auth/login',
                pathMatch: 'full'
            }*/
        ]
    }
];
