import {NgModule} from '@angular/core';
import {SharedModule} from '../shared-module/shared-module.module';
import {SearchAccountComponent} from './search-account/search-account.component';
import {AccountRoutingModule} from './account-routing.module';
import {SearchAccountService} from './services/search-account.service';
import {MainAccountComponent} from './main-account/main-account.component';
import {CommonsModule} from '../commons-module/commons-module.module';

@NgModule({
    declarations: [SearchAccountComponent, MainAccountComponent],
    imports: [
        SharedModule,
        AccountRoutingModule,
        CommonsModule
    ],
    providers: [
        SearchAccountService
    ]
})
export class AccountModule {
}
