import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ACCOUNT_ROUTES} from './account-module.routes';

@NgModule({
    imports: [RouterModule.forChild(ACCOUNT_ROUTES)],
    exports: [RouterModule]
})
export class AccountRoutingModule {
}
