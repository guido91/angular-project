import {NgModule} from '@angular/core';
import {MainContactComponent} from './main-contact/main-contact.component';
import {CreateContactComponent} from './create-contact/create-contact.component';
import {SharedModule} from '../shared-module/shared-module.module';
import {ContactRoutingModule} from './contact-routing.module';
import {CommonsModule} from '../commons-module/commons-module.module';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactItemComponent } from './contact-item/contact-item.component';
import {LoadContactService} from './services/load-contact.service';
import {CreateContactService} from './services/create-contact.service';
import {SearchContactsModel} from './models/search-contacts.model';

@NgModule({
    declarations: [MainContactComponent, CreateContactComponent, ContactListComponent, ContactItemComponent],
    imports: [
        SharedModule,
        ContactRoutingModule,
        CommonsModule
    ],
    providers: [LoadContactService, CreateContactService, SearchContactsModel]
})
export class ContactModule {
}
