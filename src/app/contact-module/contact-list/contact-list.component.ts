import {Component, OnDestroy, OnInit} from '@angular/core';
import {ContactModel} from '../../models/contact.model';
import {Subscription} from 'rxjs';
import {LoadContactService} from '../services/load-contact.service';
import {SearchContactModel} from '../../models/search-contact.model';
import {SearchContactsModel} from '../models/search-contacts.model';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit, OnDestroy {

  public contacts: ContactModel[];

  public readonly size = 3;

  private lobbyUserModelSubscription: Subscription;
  private loadcontactsSubscription: Subscription;

  constructor(private loadcontactsHttpService: LoadContactService,
              private searchContactsModel: SearchContactsModel) {
  }

  ngOnInit() {
    const requestSearch: SearchContactModel = {information: 'rolando', withoutAccountOwner: false};
    this.loadContacts(this.size, this.searchContactsModel.page, requestSearch);

    this.lobbyUserModelSubscription = this.searchContactsModel.asObservable().subscribe(
        (contacts: ContactModel[]) => {
          console.log('contacts');
          console.log(contacts);
          this.contacts = contacts;
        }
    );
  }

  ngOnDestroy(): void {
    this.searchContactsModel.init();

    this._unsubscribe(this.loadcontactsSubscription);
    this._unsubscribe(this.lobbyUserModelSubscription);
  }

  public loadContacts(limit: number, page: number, request: SearchContactModel): void {
    console.log('loadContacts');
    this.loadcontactsSubscription = this.loadcontactsHttpService.doPost(limit, page, request).subscribe(
        (response: any) => {
          console.log('model');
          console.log(response);
          this.searchContactsModel.page++;

          const contacts = response.content;
          this.searchContactsModel.addContacts(contacts);
        }
    );
  }

  public onScroll(): void {
    const requestSearch: SearchContactModel = {information: 'rolando', withoutAccountOwner: false};
    this.loadContacts(this.size, this.searchContactsModel.page, requestSearch);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
