import {Injectable, Injector} from '@angular/core';
import {AuthModel} from '../../models/auth.model';
import {Observable} from 'rxjs';
import {HttpService} from '../../../bootstrap/http.service';
import {HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from '../../shared-module/services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class CreateContactService extends HttpService {

    constructor(private _injector: Injector,
                private authService: AuthenticationService) {
        super();
    }

    protected injector(): Injector {
        return this._injector;
    }

    path(): string {
        return '/api/contact/secure/contacts';
    }

    public doPost(request: AuthModel): Observable<any> {
        const headers = new HttpHeaders({
            'Account-ID': this.authService.getAccountId(),
            'User-ID': this.authService.getUserId()
        });
        return this.httpClient().post(this.getUrl(), request, {observe: 'response', headers});
    }
}
