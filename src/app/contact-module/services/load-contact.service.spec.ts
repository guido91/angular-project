import { TestBed } from '@angular/core/testing';

import { LoadContactService } from './load-contact.service';

describe('LoadContactService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadContactService = TestBed.get(LoadContactService);
    expect(service).toBeTruthy();
  });
});
