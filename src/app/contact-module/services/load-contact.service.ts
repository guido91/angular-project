import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../../bootstrap/http.service';
import {Observable} from 'rxjs';
import {SearchContactModel} from '../../models/search-contact.model';
import {HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from '../../shared-module/services/authentication.service';

@Injectable()
export class LoadContactService extends HttpService {

    constructor(private _injector: Injector,
                private authService: AuthenticationService) {
        super();
    }

    public path(): string {
        return '/api/contact/secure/contacts/search';
    }

    public doPost(limit: number, page: number, request: SearchContactModel): Observable<object> {
        console.log('empieza');
        const headers = new HttpHeaders({
            'Account-ID': this.authService.getAccountId(),
            'User-ID': null,
            'Content-Type': 'application/json'
        });
        const options = { headers};
        // headers.append('Content-Type', 'application/json');
        return this.httpClient().post(`${this.getUrl()}?limit=${limit}&page=${page}`, request);
    }

    protected injector(): Injector {
        return this._injector;
    }
}
