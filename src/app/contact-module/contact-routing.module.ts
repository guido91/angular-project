import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CONTACT_ROUTES} from './contact-module.routes';

@NgModule({
    imports: [RouterModule.forChild(CONTACT_ROUTES)],
    exports: [RouterModule]
})
export class ContactRoutingModule {
}
