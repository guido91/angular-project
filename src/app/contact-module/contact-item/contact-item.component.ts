import {Component, Input} from '@angular/core';
import {ContactModel} from '../../models/contact.model';

@Component({
    selector: 'app-contact-item',
    templateUrl: './contact-item.component.html',
    styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent {
    @Input()
    public contact: ContactModel;
    @Input()
    public index: number;

    private readonly EMPTY = '';

    constructor() {
        this.contact = {
            id: null,
            avatarId: this.EMPTY,
            email: this.EMPTY,
            information: this.EMPTY,
            name: this.EMPTY
        };

        this.index = 0;
    }

}
