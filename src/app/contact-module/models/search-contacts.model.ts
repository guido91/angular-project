import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {ContactModel} from '../../models/contact.model';

@Injectable()
export class SearchContactsModel extends BehaviorSubject<ContactModel[]> {

    public contacts: ContactModel[];

    private _page: number;

    constructor() {
        super([]);
        this.contacts = [];

        this._page = 1;
    }

    public init(): void {
        this.contacts = [];
        this._page = 1;

        this.next([]);
    }

    public addContacts(contacts: ContactModel[]): void {
        for (const contact of contacts) {
            const found = this.contacts.find(
                (contactstored: ContactModel) => {
                    return contactstored.id === contact.id;
                }
            );

            if (!found) {
                this._pushContact(contact);
            }

        }
        this.next(this.contacts);
    }

    private _pushContact(contact: ContactModel): void {
        this.contacts.push(contact);
    }

    public get page(): number {
        return this._page;
    }

    public set page(page: number) {
        this._page = page;
    }
}
