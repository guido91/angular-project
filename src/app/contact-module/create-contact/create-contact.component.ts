import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CreateContactService} from '../services/create-contact.service';
import {ContactModel} from '../../models/contact.model';

@Component({
    selector: 'app-create-contact',
    templateUrl: './create-contact.component.html',
    styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit {

    public createContactForm: FormGroup;

    public contact: any;

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private createContactService: CreateContactService) {
        this.contact = {};
        this.createContactForm = formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            information: ['', [Validators.required]],
            avatartId: ['1', []],
        });
    }

    ngOnInit() {
    }

    onCreateContact() {
        console.log(this.createContactForm.value);
        this.createContactService.doPost(this.createContactForm.value).subscribe((response) => {
            console.log(response);
            this.contact = response;
        });
    }

}
