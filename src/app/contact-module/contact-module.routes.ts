import {Routes} from '@angular/router';
import {MainContactComponent} from './main-contact/main-contact.component';
import {CreateContactComponent} from './create-contact/create-contact.component';
import {ContactListComponent} from './contact-list/contact-list.component';
import {AuthGuard} from '../routing-module/guards/auth.guard';

export const CONTACT_ROUTES: Routes = [
    {
        path: '',
        component: MainContactComponent,
        children: [
            {
                path: 'create',
                component: CreateContactComponent,
                canLoad: [AuthGuard]
            },
            {
                path: 'list',
                component: ContactListComponent,
                canLoad: [AuthGuard]
            },
            /*{
                path: '',
                redirectTo: '/auth',
                pathMatch: 'full'
            }*/
        ]
    }
];
