export interface CompanyModel {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}
