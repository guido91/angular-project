export interface ContactModel {
    id: number;
    avatarId: string;
    email: string;
    information: string;
    name: string;
}
