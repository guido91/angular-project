export interface SearchContactModel {
    information: string;
    withoutAccountOwner: boolean;
}
