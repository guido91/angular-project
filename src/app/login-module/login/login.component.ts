import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginHttpService} from '../services/login-http.service';
import {AuthenticationService} from '../../shared-module/services/authentication.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;

    constructor(private formBuilder: FormBuilder,
                private loginHttpService: LoginHttpService,
                private authService: AuthenticationService,
                private router: Router) {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        });
    }

    ngOnInit() {
    }

    onLogin() {
        console.log(this.loginForm.value);
        if (this.loginForm.valid) {
            this.loginHttpService.doPost(this.loginForm.value).subscribe(
                (response) => {
                    console.log(response.headers.get('Authorization'));
                    this.authService.setToken(response.headers.get('Authorization'));
                    this.router.navigate(['account']);
                }
            );
        }
    }

}
