import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../../bootstrap/http.service';
import {AuthModel} from '../../models/auth.model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoginHttpService extends HttpService {

    constructor(private _injector: Injector) {
        super();
    }

    protected injector(): Injector {
        return this._injector;
    }

    path(): string {
        return '/api/security/auth';
    }

    public doPost(request: AuthModel): Observable<any> {

        return this.httpClient().post(this.getUrl(), request, {observe: 'response'});
    }
}
