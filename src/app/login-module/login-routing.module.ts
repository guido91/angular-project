import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {LOGIN_ROUTES} from './login-module.routes';

@NgModule({
    imports: [RouterModule.forChild(LOGIN_ROUTES)],
    exports: [RouterModule]
})
export class LoginRoutingModule {
}
