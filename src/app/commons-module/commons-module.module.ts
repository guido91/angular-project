import {NgModule} from '@angular/core';
import {SharedModule} from '../shared-module/shared-module.module';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [NavBarComponent],
    exports: [NavBarComponent],
    imports: [
        SharedModule,
        RouterModule
    ]
})
export class CommonsModule {
}
