import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../shared-module/services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  constructor(private authService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit() {
  }

  public logout(): void {
    this.authService.removeToken();
    this.router.navigate(['']);
  }
}
