import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {MainComponent} from './components/main/main.component';
import {SharedModule} from './shared-module/shared-module.module';
import {RoutingModule} from './routing-module/routing-module.module';
import {BrowserModule} from '@angular/platform-browser';
import {NavBarComponent} from './commons-module/nav-bar/nav-bar.component';
import {CommonsModule} from './commons-module/commons-module.module';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent
    ],
    imports: [
        BrowserModule,
        RoutingModule,
        SharedModule.forRoot(),
        CommonsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
