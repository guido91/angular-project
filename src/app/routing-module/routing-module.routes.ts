import {Routes} from '@angular/router';
import {AuthGuard} from './guards/auth.guard';

export const APP_ROUTES: Routes = [
    {
        path: 'auth',
        loadChildren: '../login-module/login-module.module#LoginModule',
    },
    {
        path: 'account',
        loadChildren: '../account-module/account-module.module#AccountModule',
        canLoad: [AuthGuard]
    },
    {
        path: 'contact',
        loadChildren: '../contact-module/contact-module.module#ContactModule',
        canLoad: [AuthGuard]
    },
    {
        path: 'company',
        loadChildren: '../company-module/company-module.module#CompanyModule',
        canLoad: [AuthGuard]
    },
    {
        path: '',
        redirectTo: '/auth',
        pathMatch: 'full'
    },
    /*{
        path: '**',
        component: NotFoundComponent
    }*/
];
