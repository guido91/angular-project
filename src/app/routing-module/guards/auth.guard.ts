import {Injectable} from '@angular/core';
import {CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../../shared-module/services/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad {
    constructor(private authService: AuthenticationService,
                private router: Router) {

    }

    canLoad(
        route: Route,
        segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        const token = this.authService.getToken();

        if (token) {
            // this.router.navigate(['account']);
            return true;
        }

        this.router.navigate(['auth']);
        return false;
    }
}
