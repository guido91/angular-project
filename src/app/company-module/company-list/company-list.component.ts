import {Component, OnDestroy, OnInit} from '@angular/core';
import {ContactModel} from '../../models/contact.model';
import {Subscription} from 'rxjs';
import {LoadContactService} from '../../contact-module/services/load-contact.service';
import {SearchContactsModel} from '../../contact-module/models/search-contacts.model';
import {SearchContactModel} from '../../models/search-contact.model';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss']
})
export class CompanyListComponent implements OnInit, OnDestroy {

  public contacts: ContactModel[];

  public readonly size = 3;

  private lobbyUserModelSubscription: Subscription;
  private loadcontactsSubscription: Subscription;

  constructor(private loadcontactsHttpService: LoadContactService,
              private searchContactsModel: SearchContactsModel) {
  }

  ngOnInit() {
    const requestSearch: SearchContactModel = {information: '', withoutAccountOwner: true};
    this.loadContacts(this.size, this.searchContactsModel.page, requestSearch);

    this.lobbyUserModelSubscription = this.searchContactsModel.asObservable().subscribe(
        (contacts: ContactModel[]) => {
          this.contacts = contacts;
        }
    );
  }

  ngOnDestroy(): void {
    this.searchContactsModel.init();

    this._unsubscribe(this.loadcontactsSubscription);
    this._unsubscribe(this.lobbyUserModelSubscription);
  }

  public loadContacts(limit: number, page: number, request: SearchContactModel): void {
    this.loadcontactsSubscription = this.loadcontactsHttpService.doPost(limit, page, request).subscribe(
        (response: any) => {
          console.log(response);
          this.searchContactsModel.page++;

          const contacts = response.data;
          this.searchContactsModel.addContacts(contacts);
        }
    );
  }

  public onScroll(): void {
    const requestSearch: SearchContactModel = {information: '', withoutAccountOwner: true};
    this.loadContacts(this.size, this.searchContactsModel.page, requestSearch);
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}

