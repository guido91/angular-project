import {Routes} from '@angular/router';
import {MainCompanyComponent} from './main-company/main-company.component';
import {CreateCompanyComponent} from './create-company/create-company.component';
import {AuthGuard} from '../routing-module/guards/auth.guard';

export const COMPANY_ROUTES: Routes = [
    {
        path: '',
        component: MainCompanyComponent,
        children: [
            {
                path: 'create',
                component: CreateCompanyComponent,
                canLoad: [AuthGuard]
            },
            /*{
                path: '',
                redirectTo: '/auth',
                pathMatch: 'full'
            }*/
        ]
    }
];
