import { TestBed } from '@angular/core/testing';

import { CreateCompanyService } from './create-company.service';

describe('CreateCompanyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateCompanyService = TestBed.get(CreateCompanyService);
    expect(service).toBeTruthy();
  });
});
