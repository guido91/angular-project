import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {CompanyModel} from '../../models/company.model';
import {HttpService} from '../../../bootstrap/http.service';

@Injectable()
export class CreateCompanyService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/users/public/companies';
  }

  public doPost(company: CompanyModel): Observable<any> {
    console.log(company);
    return this.httpClient().post(this.getUrl(), company);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
