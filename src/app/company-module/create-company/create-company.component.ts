import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CreateCompanyService} from '../services/create-company.service';

@Component({
    selector: 'app-create-company',
    templateUrl: './create-company.component.html',
    styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {

    public createCompanyForm: FormGroup;
    public company: any;
    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private createCompanyService: CreateCompanyService) {
        this.company = {};
        this.createCompanyForm = formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
            confirmPassword: ['', [Validators.required]],
        });
    }

    ngOnInit() {
    }

    onCreateCompany() {
        console.log(this.createCompanyForm.value);
        this.createCompanyService.doPost(this.createCompanyForm.value).subscribe((response) => {
            console.log(response);
            this.company = response;
        });
    }

}
