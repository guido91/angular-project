import {Component, Input, OnInit} from '@angular/core';
import {CompanyModel} from '../../models/company.model';

@Component({
  selector: 'app-company-item',
  templateUrl: './company-item.component.html',
  styleUrls: ['./company-item.component.scss']
})
export class CompanyItemComponent {
  @Input()
  public company: CompanyModel;
  @Input()
  public index: number;

  private readonly EMPTY = '';

  constructor() {
    this.company = {
      email: this.EMPTY,
      name: this.EMPTY,
      password: this.EMPTY,
      confirmPassword: this.EMPTY
    };

    this.index = 0;
  }

}
