import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {COMPANY_ROUTES} from './company-module.routes';

@NgModule({
    imports: [RouterModule.forChild(COMPANY_ROUTES)],
    exports: [RouterModule]
})
export class CompanyRoutingModule {
}
