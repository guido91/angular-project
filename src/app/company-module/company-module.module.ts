import {NgModule} from '@angular/core';
import {MainCompanyComponent} from './main-company/main-company.component';
import {CreateCompanyComponent} from './create-company/create-company.component';
import {SharedModule} from '../shared-module/shared-module.module';
import {CompanyRoutingModule} from './company-routing.module';
import {CommonsModule} from '../commons-module/commons-module.module';
import {CreateCompanyService} from './services/create-company.service';
import { CompanyListComponent } from './company-list/company-list.component';
import { CompanyItemComponent } from './company-item/company-item.component';

@NgModule({
    declarations: [MainCompanyComponent, CreateCompanyComponent, CompanyListComponent, CompanyItemComponent],
    imports: [
        SharedModule,
        CompanyRoutingModule,
        CommonsModule
    ],
    providers: [CreateCompanyService]
})
export class CompanyModule {
}
