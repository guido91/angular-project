import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthenticationService {

    private readonly PASSPHRASE = 'angular-project';
    private readonly KEY = 'token';
    private readonly ACCOUNTID = 'Account-ID';
    private readonly USERID = 'User-ID';

    constructor() {
    }

    public setToken(token: string): void {
        const tokene = token.split(' ', 2)[1];
        const decoded = jwt_decode(tokene);
        // const decodedData = decode(tokene, { header: true });
        console.log(decoded);
        localStorage.setItem(this.KEY, CryptoJS.AES.encrypt(token, this.PASSPHRASE));
        localStorage.setItem(this.ACCOUNTID, decoded.accountId);
        localStorage.setItem(this.USERID, decoded.userId);
    }

    public getToken(): string {
        let response: string;
        const encryptedToken = localStorage.getItem(this.KEY);
        if (encryptedToken) {
            const bytes = CryptoJS.AES.decrypt(encryptedToken, this.PASSPHRASE);
            response = bytes.toString(CryptoJS.enc.Utf8);
        }
        return response;
    }

    public getAccountId(): string {
        return localStorage.getItem(this.ACCOUNTID);
    }

    public getUserId(): string {
        return localStorage.getItem(this.USERID);
    }

    public removeToken(): void {
        localStorage.removeItem(this.KEY);
        localStorage.removeItem(this.ACCOUNTID);
        localStorage.removeItem(this.USERID);
    }
}
